<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Entity\Participant;
use App\Entity\Payment;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
    /**
     * @Route("/", name="campaign_index", methods="GET")
     */
    public function index(): Response
    {
        $campaigns = $this->getDoctrine()
            ->getRepository(Campaign::class)
            ->findAll();

        return $this->render('campaign/index.html.twig', ['campaigns' => $campaigns]);
    }

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        // Récupération du nom de l'auteur
        $campaign->setName($request->request->get('name'));

        if (isset($_GET['campaign_name'])) {
            $campaign_name =$_GET['campaign_name'];
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', [
                'id' => $campaign->getId()
            ]);
        }

        if (isset($_GET['campaign_name'])) {
            $campaign_name =$_GET['campaign_name'];
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
                'campaign_name' => $campaign_name
            ]);
        } else {
                return $this->render('campaign/new.html.twig', [
                    'campaign' => $campaign,
                    'form' => $form->createView(),
                ]);
        }
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $query = 'SELECT participant.*, payment.amount as payment_amount, spending.amount as spending_amount, spending.label as spending_label FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        LEFT JOIN spending ON spending.participant_id = participant.id
        WHERE campaign_id = "'.$campaign->getId() .'"';
        
        $statement =  $this->getDoctrine()
                            ->getManager()
                            ->getConnection()
                            ->prepare($query);
        $statement->execute();
        
        $participantWithParticipations = $statement->fetchAll();

        $numberParticipations = count($participantWithParticipations);

        $total_amountInCts = 0;
        for($i = 0; $i <= ($numberParticipations - 1); $i++) {
            $total_amountInCts += $participantWithParticipations[$i]['payment_amount'];
            $total_amountInCts += $participantWithParticipations[$i]['spending_amount'];
        }
        $total_amountInEuros = $total_amountInCts / 100;

        $objectif = round($total_amountInEuros / $campaign->getGoal() * 100);
        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'numberParticipations','total_amountInEuros', 'objectif'));
    }

    /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET")
     */
    public function pay(Campaign $campaign): Response
    {
        // coalesce operator
        $amount_participant = $_GET['amount_participant'] ?? "";
        return $this->render('campaign/pay.html.twig', compact('campaign', 'amount_participant'));
    }

    /**
     * @Route("/{id}/spend", name="campaign_spend_form", methods="GET")
     */
    public function spend(Campaign $campaign): Response
    {
        $spending_participant = $_GET['spending_participant'];
        return $this->render('campaign/spending.html.twig', compact('campaign', 'spending_participant'));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
