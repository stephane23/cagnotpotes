<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Campaign;
use App\Entity\Participant;
use App\Entity\Spending;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{

    /**
     * @Route("/charge", name="spending_process", methods="POST")
     */
    public function charge(Request $request)
    {       
        $campaign_id = $request->request->get('campaign_id');
        $amount = (int)$request->request->get('amount') * 100;
        $label = $request->request->get('content');

        // Instancier la campagne
        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);

        //Enregistrer le participant
        $participant = new Participant();
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

        // Enregistrer le paiement qui est dépendant du participant
        $spending = new Spending();
        $spending->setAmount($amount);
        $spending->setParticipant($participant);
        $spending->setLabel($label);
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        // Redirection vers la fiche campagne
        return $this->redirectToRoute('campaign_show', [
            'id' => $campaign_id
        ]);
    }
}
