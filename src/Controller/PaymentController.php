<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Entity\Campaign;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{

    /**
     * @Route("/charge", name="payment_charge", methods="POST")
     */
    public function charge(Request $request): Response
    {       
        //Payer avec Stripe
        try {
            $amount = (int)$request->request->get('amount') * 100;
            $token = $request->request->get("stripeToken");

            \Stripe\Stripe::setApiKey('sk_live_5MQONbxawvFN2L2jbReraG3z');
            $charge = \Stripe\Charge::create([
                'amount' => $amount, 
                'currency' => 'eur',
                'source' => $token,
            ]);
        }
        catch(\Exception $e) {
            $this->addFlash(
                'error',
                'Le paiement a échoué. Raison : '.$e->getMessage()
            );
            return $this->redirectToRoute('campaign_pay', [
                'id' => $campaign_id
            ]);
        }

        $campaign_id = $request->request->get('campaign_id');

        // Instancier la campagne
        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);

        //Enregistrer le participant
        $participant = new Participant();
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

        // Enregistrer le paiement qui est dépendant du participant
        $payment = new Payment();
        $payment->setAmount($amount);
        $payment->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        // Redirection vers la fiche campagne
        return $this->redirectToRoute('campaign_show', [
            'id' => $campaign_id
        ]);
    }
}
